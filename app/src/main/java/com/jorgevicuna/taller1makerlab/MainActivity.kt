package com.jorgevicuna.taller1makerlab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var nightMode : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        Toast.makeText(applicationContext,"onCreate", Toast.LENGTH_SHORT).show()
        Log.i("MainActivity", "onCreate")


        siguienteActivity.setOnClickListener {
            val textoIngresado = palabra_ingresada.text.toString()
            val intent = Intent(this, ResultadoActivity::class.java)
            intent.putExtra("palabra_ingresada", textoIngresado)
            startActivity(intent)
        }
    }


    override fun onStart() {
        super.onStart()
        Toast.makeText(applicationContext,"onStart",Toast.LENGTH_SHORT).show()

        Log.i("MainActivity", "onStart")
    }

    override fun onResume() {
        super.onResume()

        Toast.makeText(applicationContext,"onResume",Toast.LENGTH_SHORT).show()
        Log.i("MainActivity", "onResume")
    }

    override fun onPause() {
        super.onPause()

        Toast.makeText(applicationContext,"onPause",Toast.LENGTH_SHORT).show()
        Log.i("MainActivity", "onPause")
    }

    override fun onStop() {
        super.onStop()

        Toast.makeText(applicationContext,"onStop",Toast.LENGTH_SHORT).show()
        Log.i("MainActivity", "onStop")
    }


    override fun onDestroy() {
        super.onDestroy()

        Toast.makeText(applicationContext,"onDestroy",Toast.LENGTH_SHORT).show()
        Log.i("MainActivity", "onDestroy")
    }
}
